# Introduction

After years of study, you finally landed an opportunity to start your academic career. You know the important concepts in your field of study, you mastered the mathematical formulas you had to learn (or joyfully discovered, if that's your cup of tea).

Here comes the first task: "Could you write me a program to...".

**What?!** Programming? But I thought I would do `<fill in your field of study>`! You dabbled with some code for some exams, but you never liked programing: it's too confusing!!

Have you ever found yourself in this situation? Then, _this is the guide for you!_

In this book, I'll give you the essential knowledge, principles, and tools that will help you write better programs. The goal is for you to spend less time stuck in messy code that doesn't work, and more time on the focus of your research.

## Who is this for?

This book is addressed to all those who are not professional programmers, but have to program nonetheless. While the main focus is for people working in a research setting (PhDs, scientists, student assistants, etc.), the principles are broad and easily applicable to other fields as well.

While some programming experience is recommended, the explanations do not assume deep knowledge of any kind.

## Topics covered in this book

The examples will be mainly written in [Python](https://www.python.org), but equivalent solutions in [R](https://www.r-project.org/), [Javascript](https://en.wikipedia.org/wiki/JavaScript) or other languages will be added where possible. However, the goal it talk about _principles_ and not strictly about the specifics. Most things should be easily transferrable to whatever programming language you will be working with.

## Chapter overview
